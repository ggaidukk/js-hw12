const btn = document.querySelectorAll(".btn");

document.addEventListener("keyup", (e) => {
  btn.forEach((button) => {
    if (
      e.key === button.textContent ||
      e.key === button.textContent.toLowerCase()
    ) {
      button.classList.add("tap");
    }
  });
});
document.addEventListener("keydown", (e) => {
  btn.forEach((button) => {
    if (
      e.key !== button.textContent ||
      e.key !== button.textContent.toLowerCase()
    ) {
      button.classList.remove("tap");
    }
  });
});
